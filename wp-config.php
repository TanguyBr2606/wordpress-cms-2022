<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'JUVT%#.]iRGFmBQv_M~{5at!F*[[Upy4ZzSKliIEIL(?&qKz7#O{G[.>&~&GWeoj' );
define( 'SECURE_AUTH_KEY',  'x#APvN9H_HZ2Wg*kbCbiL@[OtDaKU$KbY&cyF+g@G8ie?DNpDR%[6EK 2uz`ih-N' );
define( 'LOGGED_IN_KEY',    'MFDHEA/;VAu&MUgi~4^]wu$vuhzD?9@I;Lkxwi#r79q}@?{*[oK{|bE#j|7jbnNP' );
define( 'NONCE_KEY',        'RV+hQa%uR5<$QGCNs9w]}-K9JsNIp 7%zH3M!s[k=,w0f7M6jb@sbVI9Tg,uZ6FY' );
define( 'AUTH_SALT',        '` ijI)Ku+|{S#oa_vo`jezmG4L{Y<SZs]k:A`#T#8M]>nD#_py@qMC.QNhqwd^(<' );
define( 'SECURE_AUTH_SALT', '^C<_(D)u>KOu B=LgPSb7V0S;hs1#nHAPg)0:DMF41g#zF7RKpdMT9gge},E`w[/' );
define( 'LOGGED_IN_SALT',   'zf+&jfP9DfovL*H0keX}!cC_>gc[CO]o3zp~:KgyQutQ+udJCTiQl^`52,y!inbp' );
define( 'NONCE_SALT',       'zG&sbxx|7iFI8Jne<(Pjrb*[R+PYb9dD&Z&Ft#U/&H..nNN= :96A!$2!fC,lN%J' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
