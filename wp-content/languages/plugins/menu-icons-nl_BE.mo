��    7      �  I   �      �     �  4   �     �  B     D   S     �     �     �     �     �     �     �  .   �  
   �  	     	             "  
   2     =  
   B     M  
   S     ^  T   d     �  
   �     �  +   �            �        �     �     �     �  	                  #  9   /     i     m     s       	   �  	   �     �     �     �     �  )   �     �     �  /  	     7
  A   N
     �
  @   �
  C   �
     /     2  	   7     A     F     O     V  :   c  	   �     �     �     �     �     �     �                     &  Y   ,     �  
   �     �  5   �     �     �  �   �     �  	   �     �     �            
   #     .  7   >     v     z     �     �  	   �  	   �     �     �     �     �  )   �                     $   +       -      !   4       .      3         6                       *   0       7      /   )   5       1      2            
       	              #                      "   '                         &   ,         %                         (           "%s" menu settings %1$s: Type %2$s is not supported, reverting to text. &mdash; Select &mdash; <strong>Menu Icons Settings</strong> have been successfully reset. <strong>Menu Icons Settings</strong> have been successfully updated. After All Baseline Before Bottom Change Current Menu Discard all changes and reset to default state Extensions Font Size Full Size Global Global settings Hide Label Icon Icon Types Icon: Image Size Large Looks like Menu Icons was installed via Composer. Please activate Icon Picker first. Medium Menu Icons Menu Icons Settings Menu Icons: No registered icon types found. Middle No Please note that the actual look of the icons on the front-end will also be affected by the style of your active theme. You can add your own CSS using %2$s or a plugin such as %3$s if you need to override it. Position Preview Remove Reset SVG Width Save Settings Select Select Icon Spice up your navigation menus with pretty icons, easily. Sub Super Text Bottom Text Top ThemeIsle Thumbnail Top Type Vertical Align Yes https://github.com/Codeinwp/wp-menu-icons https://themeisle.com the customizer PO-Revision-Date: 2022-01-13 09:16:22+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/4.0.0-alpha.3
Language: nl_BE
Project-Id-Version: Plugins - Menu Icons by ThemeIsle - Development (trunk)
 "%s" menu instellingen %1$s: Type %2$s wordt niet ondersteund en keert terug naar tekst. &mdash; Selecteer &mdash; <strong>Menu Icons instellingen</strong> zijn succesvol gereset. <strong>Menu Icons instellingen</strong> zijn succesvol bijgewerkt. Na Alle Basislijn Voor Onderaan Wijzig Huidige menu Gooi alle wijzigingen weg en reset naar de standaardstatus Extensies Lettertype grootte Volledige grootte Globaal Globale instellingen Label verbergen Icoon Icoon types Icoon: Afbeeldingsgrootte Groot Het lijkt erop dat Menu Icons is geïnstalleerd via Composer. Activeer eerst Icon Picker. Medium Menu Icons Menu Icons instellingen Menu Icons: geen geregistreerde icoon types gevonden. Midden Nee Houd er rekening mee dat het daadwerkelijke uiterlijk van de iconen aan de voorkant ook wordt beïnvloed door de stijl van je actieve thema. Je kan je eigen CSS toevoegen met %2$s of een plugin zoals %3$s als je deze moet overschrijven. Positie Voorbeeld Verwijderen Reset SVG breedte Instellingen opslaan Selecteren Selecteer icoon Verfraai je navigatiemenu's eenvoudig met mooie iconen. Sub Super Tekst onderaan Tekst bovenaan ThemeIsle Thumbnail Bovenaan Type Verticale uitlijning Ja https://github.com/Codeinwp/wp-menu-icons https://themeisle.com de customizer 