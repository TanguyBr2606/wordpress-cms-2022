��          �            h     i       4   �     �  
   �     �                >     W  :   f  A   �  -   �       *       I     `  <   }     �     �     �       !        7     S     b  A   q  -   �     �                                
                                            	    Could not switch off. Could not switch users. Instant switching between user accounts in WordPress John Blackbourn & contributors Switch Off Switch back to %1$s (%2$s) Switch&nbsp;To Switched back to %1$s (%2$s). Switched to %1$s (%2$s). User Switching User Switching title on user profile screenUser Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ verbSponsor PO-Revision-Date: 2022-06-26 10:29:49+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/4.0.0-alpha.1
Language: nl_BE
Project-Id-Version: Plugins - User Switching - Stable (latest release)
 Kon niet uitschakelen. Kon gebruiker niet wisselen. Onmiddellijk wisselen tussen gebruikersaccounts in WordPress John Blackbourn & contributors Uitschakelen Wissel terug naar %1$s (%2$s) Wissel&nbsp;Naar Terug gewisseld naar %1$s (%2$s). Gewisseld naar %1$s (%2$s). User Switching User Switching https://github.com/johnbillion/user-switching/graphs/contributors https://wordpress.org/plugins/user-switching/ Sponsor 